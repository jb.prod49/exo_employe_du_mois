import sqlite3
from sqlite3 import Error
import openpyxl
from datetime import time, date, datetime

conn = sqlite3.connect("bdd.db")
cursor = conn.cursor()
#--------------------------------------------------#
#------- Déclaration des fonctions pour BDD -------#
#--------------------------------------------------#
def execute_query(query: str, cursor, params=None, fetch=True):
    try:
        cursor.execute(query, params or ())
        result = cursor.fetchall() if fetch else None
        return result
    except Error as e:
        print(f"Erreur lors de l'exécution de la requête : {e}")
        print(f"query : {query}")
        return None

def execute_insert(query: str, values: tuple, cursor)->int:
    try:
        cursor.execute(query, values)
        return cursor.lastrowid
    # except Error as e:
    #     print(f"Erreur lors de l'exécution de la requête : {e}")
    #     print(f"query : {query}")
    #     return None
    except sqlite3.IntegrityError:
        return None
    
#------------------------------------------------------#
#------- Déclaration des fonctions pour SQLITE3 -------#
#------------------------------------------------------#
def add_department(cursor,department_name:str)-> int:
    query = "INSERT INTO department(department_name) VALUES (?)"
    return execute_insert(query,(department_name,),cursor)

def add_location(cursor, location_name:str)-> int:
    query = "INSERT INTO location(location_name) VALUES (?)"
    return execute_insert(query,(location_name,),cursor)

def add_employee(cursor, employee_name: str, department_name:str, employee_id:int = None)->int:
    if verif_int(employee_id) == True :
        id_department = add_department(cursor, department_name)
        if not id_department:
            id_department = execute_query("SELECT department_id FROM department WHERE department_name = (?)", cursor, (department_name,))
            if id_department:
                id_department = id_department[0][0]
        if employee_id:
            query = "INSERT INTO employee(employee_id, employee_name, department) VALUES (?,?,?)"
            id_employee = execute_insert(query,(employee_id,employee_name,id_department),cursor)
        else:
            query = "INSERT INTO employee(employee_name, department) VALUES (?,?)"
            id_employee = execute_insert(query,(employee_name,id_department),cursor)
        return id_employee

def add_working_day(cursor, shift_start_time:str, shift_end_time:str, break_duration:int, overtime_hours:int, location:str)->int:
    if verif_int(break_duration) and verif_int(overtime_hours):
        id_location = add_location(cursor, location)
        if not id_location:
            id_location = execute_query("SELECT location_id FROM location WHERE location_name = (?)", cursor, (location,))
            if id_location:
                id_location = id_location[0][0]
        shift_start_time = normalize_time(shift_start_time)
        shift_end_time = normalize_time(shift_end_time)
        query = "INSERT INTO working_day(shift_start_time, shift_end_time, break_duration, overtime_hours, location) VALUES (?,?,?,?,?)"
        id_working_day = execute_insert(query, (shift_start_time, shift_end_time, break_duration, overtime_hours, id_location), cursor)
        return id_working_day


def add_employee_working_day(cursor, employee_id:int, working_day_id:int, working_day_date:date):
    if verif_int(employee_id) == True or verif_int(working_day_id) == True:
        working_day_date = normaliser_date(working_day_date)
        query = "INSERT INTO employee_working_day(employee_id, working_day_id, working_day_date) VALUES (?,?,?)"
        return execute_insert(query,(employee_id, working_day_id, working_day_date),cursor)

#----------------------------------------------#
#-------- Fonctions de normalisation ----------#
#----------------------------------------------#
def time_format_in_object(time_str: str) -> time:
    return datetime.strptime(time_str, "%H:%M").time()

def time_format_in_str(time_object: time)-> str:
    return time_object.strftime("%H:%M")

def normalize_time (times):
    return time_format_in_str(time_format_in_object(times))

def normaliser_date(date_str: str) -> str:
    date_objet = datetime.strptime(date_str, "%d-%m-%Y")
    date_normalisee = date_objet.strftime("%Y-%m-%d")
    return date_normalisee

def verif_int(param_int)->bool:
    if isinstance(param_int, int):
        return True
    return False

#---------------------------------------------------#
#-------- création de la BDD avec SQLITE3 ----------#
#---------------------------------------------------#
print("#------ Création de la BDD ------#")
list_query = [
        """
            DROP TABLE IF EXISTS `department`;
        """,
        """
            CREATE TABLE `department` (
                `department_id` integer PRIMARY KEY NOT NULL,
                `department_name` varchar(200) UNIQUE NOT NULL);
        """,
        """
            DROP TABLE IF EXISTS `employee`;
        """,        
        """                
            CREATE TABLE `employee` (
                `employee_id` integer PRIMARY KEY,
                `employee_name` varchar(255) NOT NULL,
                `department` integer,
                FOREIGN KEY (`department`) REFERENCES `department` (`department_id`));
        """,
        """
            DROP TABLE IF EXISTS `location`;
        """,        
        """                
            CREATE TABLE `location` (
                `location_id` integer PRIMARY KEY,
                `location_name` varchar(200) UNIQUE NOT NULL);
        """,
        """
            DROP TABLE IF EXISTS `working_day`;
        """,
        """
            CREATE TABLE `working_day` (
                `working_day_id` integer PRIMARY KEY,
                `shift_start_time` time,
                `shift_end_time` time,
                `break_duration` integer,
                `overtime_hours` integer,
                `location` integer,
                FOREIGN KEY (`location`) REFERENCES `location` (`location_id`));
        """,
        """
            DROP TABLE IF EXISTS `employee_working_day`;
        """,
        """        
            CREATE TABLE `employee_working_day` (
                `employee_id` integer NOT NULL,
                `working_day_id` integer NOT NULL,
                `working_day_date` date NOT NULL,
                FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`),
                FOREIGN KEY (`working_day_id`) REFERENCES `working_day` (`working_day_id`));
        """]

for query in list_query:
    execute_query(query, cursor)
print("#------ Fin de la création ------#")

#------------------------------------------------------------#
#-------- Importation des données du fichier excel ----------#
#------------------------------------------------------------#
path_excel = "ressources/employee-65f983e1d32eb892857506.xlsx"
myBook = openpyxl.load_workbook(path_excel)

# Obtenir la liste des noms de feuilles
sheet_names = myBook.sheetnames

for sheet_name in sheet_names: 
    mySheet = myBook[sheet_name]
    if sheet_name == "employees":
        for row in mySheet.iter_rows(values_only=True):
            add_employee(cursor,row[1],row[2],row[0])
    else:
        for row in mySheet.iter_rows(min_row=3, values_only=True):
            if verif_int(row[0]) == True:
                if row[5] == "férié":
                    print(sheet_name)
                    print(row)
                id_working_day = add_working_day(cursor,row[1],row[2],row[3],row[4],row[5])
                add_employee_working_day(cursor, row[0], id_working_day, sheet_name)
conn.commit()
conn.close() 