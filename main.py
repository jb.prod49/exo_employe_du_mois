import sqlite3
from sqlite3 import Error
import openpyxl
from scipy import stats
import scipy

conn = sqlite3.connect("bdd.db")
cursor = conn.cursor()
#-----------------------------------------#
#------- Déclaration des fonctions -------#
#-----------------------------------------#
def execute_query(query: str, cursor, params=None, fetch=True):
    try:
        cursor.execute(query, params or ())
        result = cursor.fetchall() if fetch else None
        return result
    except Error as e:
        print(f"Erreur lors de l'exécution de la requête : {e}")
        print(f"query : {query}")
        return None

def checking_relevance_of_data(work_time:int, break_time:int)->bool:
    if work_time <= 0 or work_time - break_time <= 0:
        return False
    return True

def select_usine(cursor)-> list:
    query = "SELECT location_id FROM location"
    return execute_query(query, cursor)

#-----------------------------------------------------------------------#
#------- Trouver les employers qui déclare de fausse heures supp -------#
#-----------------------------------------------------------------------#
query = """
            SELECT
                ewd.employee_id,
                em.employee_name,
                (strftime('%H', shift_end_time) * 60 + strftime('%M', shift_end_time)) -
                    (strftime('%H', shift_start_time) * 60 + strftime('%M', shift_start_time))
                    AS time_difference_in_minutes,
                break_duration,
                overtime_hours*60 AS overtime_minute
            FROM
                working_day AS wd
            JOIN
                employee_working_day AS ewd
                    ON ewd.working_day_id = wd.working_day_id
            JOIN
                employee AS em
                    ON em.employee_id = ewd.employee_id;
        """        

table = execute_query(query, cursor) 
table_of_liar = []
table_name_of_liar = []
irrelevant_data = []

for row in table:
    verif = checking_relevance_of_data(row[2],row[3])
    if verif == True:
        time_real = row[2] - (row[3] + row[4])
        if time_real < 0:
            if row[0] not in [liar['employee_id'] for liar in table_name_of_liar]:
                temp2 = {"employee_id": row[0], "name_employee": row[1]}
                table_name_of_liar.append(temp2)
            temp = {"employee_id":row[0], "name_employee":row[1], "work_time":row[2], "break_duration":row[3], "declared_overtime": row[4],  "TimeCalculations":time_real}
            table_of_liar.append(temp)
    else:
        temp = {"employee_id":row[0], "name_employee":row[1], "work_time":row[2], "break_duration":row[3], "declared_overtime": row[4],  "TimeCalculations":time_real}
        irrelevant_data.append(temp)

#------- Creer fichier excel des employers qui déclare de fausse heures supp -------#
wb = openpyxl.Workbook()
sheet = wb.active
sheet.title = 'incorrect_information_list'

sheet.append(["Employee ID", "Employee Name", "Work Time (min)", "Break Duration (min)", "Declared Overtime (min)", "Time Calculations (min)"])

for liar in table_of_liar:
    sheet.append([
        liar["employee_id"],
        liar["name_employee"],
        liar["work_time"],
        liar["break_duration"],
        liar["declared_overtime"],
        liar["TimeCalculations"]
    ])

sheet1 = wb.create_sheet('list_of_lying_employees',1)
sheet1.append(["Employee ID", "Employee Name"])

for liar in table_name_of_liar:
    sheet1.append([
        liar["employee_id"],
        liar["name_employee"]
    ])

sheet2 = wb.create_sheet('list_of_irrelevant_data',0)
sheet2.append(["Employee ID", "Employee Name", "Work Time (min)", "Break Duration (min)", "Declared Overtime (min)", "Time Calculations (min)"])

for liar in irrelevant_data:
    sheet2.append([
        liar["employee_id"],
        liar["name_employee"],
        liar["work_time"],
        liar["break_duration"],
        liar["declared_overtime"],
        liar["TimeCalculations"]
    ])

filename = "justification/Proof_of_liars.xlsx"
wb.save(filename)

#--------------------------------------------------------------------------#
#------- Liste des employés ayant eu au moins une journée d’absence -------#
#--------------------------------------------------------------------------#
query = """
            SELECT 
                ewd.employee_id, 
                em.employee_name, 
                count(ewd.working_day_id) AS nb_days 
            FROM 
                employee_working_day AS ewd 
            JOIN employee AS em 
                ON em.employee_id = ewd.employee_id 
            GROUP BY 
                ewd.employee_id 
            HAVING 
                nb_days < 30;
        """
list_employees_with_absences = execute_query(query, cursor)
list_of_absentee_employees = []
for employee in list_employees_with_absences:
    # print(employee)
    query = """
                WITH 
                    date_series 
                AS 
                    (SELECT DISTINCT 
                        working_day_date 
                    FROM 
                        employee_working_day AS ewd)
                                        
                SELECT 
                    ds.working_day_date 
                FROM 
                    date_series AS ds 
                LEFT JOIN 
                    employee_working_day ewd 
                        ON ds.working_day_date = ewd.working_day_date 
                        AND ewd.employee_id = ?
                WHERE 
                    ewd.employee_id IS NULL;
            """
    date = execute_query(query,cursor, (employee[0],))
    temp = {"employee_id": employee[0], "name_employee": employee[1], "date_of_absence": date[0]}
    list_of_absentee_employees.append(temp)
print(list_of_absentee_employees)

#------------------------------------------------------------------------#
#------- Liste des 10 employés ayant le plus haut salaire de base -------#
#------------------------------------------------------------------------#
print("###### Liste des 10 employés ayant le plus haut salaire de base ######")
query = """
            WITH liar AS (
                       SELECT
                            ewd.employee_id,
                            (strftime('%H', wd.shift_end_time) * 60 + strftime('%M', wd.shift_end_time)) - 
                            (strftime('%H', wd.shift_start_time) * 60 + strftime('%M', wd.shift_start_time)) -
                            (wd.break_duration + (wd.overtime_hours * 60)) AS work_time
                        FROM 
                            working_day AS wd 
                        JOIN
                            employee_working_day AS ewd ON ewd.working_day_id = wd.working_day_id
                        WHERE work_time <= 0
                    ),
            absent AS (
                            SELECT 
                                ewd.employee_id, 
                                em.employee_name, 
                                count(ewd.working_day_id) AS nb_days 
                            FROM 
                                employee_working_day AS ewd 
                            JOIN employee AS em 
                                ON em.employee_id = ewd.employee_id 
                            GROUP BY 
                                ewd.employee_id 
                            HAVING 
                                nb_days < 30
                         ),
            false_data AS (
                   SELECT
                        wd.working_day_id,
                        (strftime('%H', wd.shift_end_time) * 60 + strftime('%M', wd.shift_end_time)) - 
                        (strftime('%H', wd.shift_start_time) * 60 + strftime('%M', wd.shift_start_time)) AS work_time
                    FROM 
                        working_day AS wd 
                    JOIN
                        employee_working_day AS ewd ON ewd.working_day_id = wd.working_day_id
                    WHERE work_time < 0 
                )
            SELECT
                ewd.employee_id,
                em.employee_name,
                SUM((strftime('%H', wd.shift_end_time) * 60 + strftime('%M', wd.shift_end_time)) - 
                (strftime('%H', wd.shift_start_time) * 60 + strftime('%M', wd.shift_start_time)) - 
                wd.break_duration - wd.overtime_hours) AS work_time,
                SUM((strftime('%H', wd.shift_end_time) * 60 + strftime('%M', wd.shift_end_time)) - 
                (strftime('%H', wd.shift_start_time) * 60 + strftime('%M', wd.shift_start_time)) - 
                wd.break_duration - wd.overtime_hours) * (?) AS month_salaire,
                overtime_hours * (?) AS overtime_amount,
                de.department_name,
                SUM((strftime('%H', wd.shift_end_time) * 60 + strftime('%M', wd.shift_end_time)) - 
                (strftime('%H', wd.shift_start_time) * 60 + strftime('%M', wd.shift_start_time)) - 
                wd.break_duration) AS work_time_whith_overtime_amount
            FROM 
                working_day AS wd
            JOIN
                employee_working_day AS ewd ON ewd.working_day_id = wd.working_day_id
            JOIN
                employee AS em ON em.employee_id = ewd.employee_id
            JOIN
                department AS de ON de.department_id = em.department
            WHERE
                ewd.employee_id NOT IN (SELECT employee_id FROM liar)
                AND
                ewd.employee_id NOT IN (SELECT employee_id FROM absent) 
                AND
                wd.working_day_id NOT IN (SELECT working_day_id FROM false_data)  
                AND 
                de.department_name = ?                 
            GROUP BY
                ewd.employee_id, em.employee_name, de.department_name
            ORDER BY 
                month_salaire DESC;	
        """

salary_list = [((12/60),(12*1.25),"Production"),((55/60),(55*1.25),"Management"),((20/60),(20*1.25),"Support"),((30/60),(30*1.25),"RD")]
salary_list_of_employees = []
for salary in salary_list:   
    list = execute_query(query, cursor, salary)
    salary_list_of_employees = salary_list_of_employees + list

query = """
            SELECT 
                lo.location_name,
                count(ewd.employee_id) AS nb_day
            FROM
                employee as em
            JOIN
                employee_working_day as ewd on ewd.employee_id =em.employee_id
            JOIN
                working_day as wd on wd.working_day_id = ewd.working_day_id
            JOIN
                location as lo on lo.location_id = wd.location
            WHERE
                ewd.employee_id = (?)
            GROUP BY
                lo.location_name;
        """

list_factory_prime = [(15,"Usine A"),(10,"Usine B"),(20,"Usine C")]
salary_list_employees_with_prime = []
for salary in salary_list_of_employees:
    result = execute_query(query,cursor,(salary[0],))
    prime_factory = (result[0][1] * list_factory_prime[0][0]) + (result[1][1] * list_factory_prime[1][0]) + (result[2][1] * list_factory_prime[1][0])
    temp = {"employe_id":salary[0],"employe_name":salary[1],"department":salary[5],"work_time":salary[6],"base_salary":salary[3],"overtim_pay":salary[4],"prime_factory":prime_factory,"total_salary":(salary[3]+salary[4]+prime_factory)}
    salary_list_employees_with_prime.append(temp)

Top_10_salary_more_pay = sorted(salary_list_employees_with_prime, key=lambda item: item ["total_salary"],reverse=True)[:10]

for salary in Top_10_salary_more_pay:
    print(salary)

#---------------------------------------------------------------------------------------#
#------- liste des 10 employés qui ont travaillé le plus d’heure et leur salaire -------#
#---------------------------------------------------------------------------------------#
print("###### liste des 10 employés qui ont travaillé le plus d’heure et leur salaire ######")
Top_10_salary_more_hours = sorted(salary_list_employees_with_prime, key=lambda item: item ["work_time"],reverse=True)[:10]
for x in Top_10_salary_more_hours:
    print(x["employe_id"],x["employe_name"],x["department"],x["work_time"],x["total_salary"])
    
#----------------------------------------------------------------------------------------#
#------- liste des 10 employés qui ont travaillé le moins d’heure et leur salaire -------#
#----------------------------------------------------------------------------------------#
print("###### liste des 10 employés qui ont travaillé le moins d’heure et leur salaire ######")
Top_10_salary_least_hours = sorted(salary_list_employees_with_prime, key=lambda item: item ["work_time"])[:10]
for x in Top_10_salary_least_hours:
    print(x["employe_id"],x["employe_name"],x["department"],x["work_time"],x["total_salary"],)

#---------------------------------------------------------------------------#
#------- Liste des 10 employés qui ont été le plus de temps en pause -------#
#---------------------------------------------------------------------------#
print("###### Liste des 10 employés qui ont été le plus de temps en pause. ######")
query = """
            WITH liar AS (
                       SELECT
                            ewd.employee_id,
                            (strftime('%H', wd.shift_end_time) * 60 + strftime('%M', wd.shift_end_time)) - 
                            (strftime('%H', wd.shift_start_time) * 60 + strftime('%M', wd.shift_start_time)) -
                            (wd.break_duration + (wd.overtime_hours * 60)) AS work_time
                        FROM 
                            working_day AS wd 
                        JOIN
                            employee_working_day AS ewd ON ewd.working_day_id = wd.working_day_id
                        WHERE work_time <= 0
                    ),
            absent AS (
                            SELECT 
                                ewd.employee_id, 
                                em.employee_name, 
                                count(ewd.working_day_id) AS nb_days 
                            FROM 
                                employee_working_day AS ewd 
                            JOIN employee AS em 
                                ON em.employee_id = ewd.employee_id 
                            GROUP BY 
                                ewd.employee_id 
                            HAVING 
                                nb_days < 30
                         )
            SELECT
                ewd.employee_id,
                em.employee_name,
                de.department_name,
                SUM(wd.break_duration) As sum_break
            FROM
                employee_working_day as ewd
            JOIN
                working_day as wd
                    on wd.working_day_id = ewd.working_day_id
            JOIN
                employee as em
                    on em.employee_id = ewd.employee_id
            JOIN
                department as de
                    on de.department_id = em.department
            WHERE
                ewd.employee_id NOT IN (SELECT employee_id FROM liar)  
                AND
                ewd.employee_id NOT IN (SELECT employee_id FROM absent)
            GROUP BY 
                ewd.employee_id
        """

Top_10_salary_more_break_time = execute_query(query, cursor)
Top_10_salary_more_break_time = sorted(Top_10_salary_more_break_time, key=lambda item: item [3],reverse=True)[:10]

for x in Top_10_salary_more_break_time:
    print(x)

#---------------------------------------------------------------------------------#
#------- Liste des employées qui ont fait le plus d’heures supplémentaires -------#
#---------------------------------------------------------------------------------#
print("###### Liste des employées qui ont fait le plus d’heures supplémentaires ######")
query = """
                    WITH liar AS (
                       SELECT
                            ewd.employee_id,
                            (strftime('%H', wd.shift_end_time) * 60 + strftime('%M', wd.shift_end_time)) - 
                            (strftime('%H', wd.shift_start_time) * 60 + strftime('%M', wd.shift_start_time)) -
                            (wd.break_duration + (wd.overtime_hours * 60)) AS work_time
                        FROM 
                            working_day AS wd 
                        JOIN
                            employee_working_day AS ewd ON ewd.working_day_id = wd.working_day_id
                        WHERE work_time <= 0
                    ),
					absent AS (
                            SELECT 
                                ewd.employee_id, 
                                em.employee_name, 
                                count(ewd.working_day_id) AS nb_days 
                            FROM 
                                employee_working_day AS ewd 
                            JOIN employee AS em 
                                ON em.employee_id = ewd.employee_id 
                            GROUP BY 
                                ewd.employee_id 
                            HAVING 
                                nb_days < 30
                         )
            SELECT
                ewd.employee_id,
                em.employee_name,
                de.department_name,
                SUM(wd.overtime_hours) As sum_overtime_hours
            FROM
                employee_working_day as ewd
            JOIN
                working_day as wd
                    on wd.working_day_id = ewd.working_day_id
            JOIN
                employee as em
                    on em.employee_id = ewd.employee_id
            JOIN
                department as de
                    on de.department_id = em.department
            WHERE
                ewd.employee_id NOT IN (SELECT employee_id FROM liar)  
                AND
                ewd.employee_id NOT IN (SELECT employee_id FROM absent)
            GROUP BY 
                ewd.employee_id
            ORDER BY 
				sum_overtime_hours DESC
			LIMIT 10
        """
Top_10_salary_more_overtime_hours = execute_query(query, cursor)
for x in Top_10_salary_more_overtime_hours:
    print(x)

#---------------------------------------------------------------------------------#
#------- Liste des employées qui ont fait le plus d’heures supplémentaires -------#
#---------------------------------------------------------------------------------#
print("###### Liste des employées qui ont fait le plus d’heures supplémentaires ######")
query = """ 
            WITH liar AS (
                    SELECT
                        ewd.employee_id,
                        (strftime('%H', wd.shift_end_time) * 60 + strftime('%M', wd.shift_end_time)) - 
                        (strftime('%H', wd.shift_start_time) * 60 + strftime('%M', wd.shift_start_time)) -
                        (wd.break_duration + (wd.overtime_hours * 60)) AS work_time
                    FROM 
                        working_day AS wd 
                    JOIN
                        employee_working_day AS ewd ON ewd.working_day_id = wd.working_day_id
                    WHERE work_time <= 0
                ),
                absent AS (
                        SELECT 
                            ewd.employee_id, 
                            em.employee_name, 
                            count(ewd.working_day_id) AS nb_days 
                        FROM 
                            employee_working_day AS ewd 
                        JOIN employee AS em 
                            ON em.employee_id = ewd.employee_id 
                        GROUP BY 
                            ewd.employee_id 
                        HAVING 
                            nb_days < 30
            )

            SELECT
                lo.location_name,
                count(wd.location)
            FROM 
                working_day as wd
            JOIN
                location as lo
                    on lo.location_id = wd.location
            JOIN
                employee_working_day as ewd
                    on ewd.working_day_id = wd.working_day_id
            WHERE
                ewd.employee_id NOT IN (SELECT employee_id FROM liar)  
                AND
                ewd.employee_id NOT IN (SELECT employee_id FROM absent)
            GROUP BY
                location
            ORDER BY
                lo.location_name
        """
distribution_of_days_by_factory = execute_query(query, cursor)
for x in distribution_of_days_by_factory:
    print(x)
    
#------------------------------------------------------------------------#
#------- Répartition des heures de travail en fonction des usines -------#
#------------------------------------------------------------------------#
print("###### Répartition des heures de travail en fonction des usines ######")   
query = """
            WITH liar AS (
                    SELECT
                        ewd.employee_id,
                        (strftime('%H', wd.shift_end_time) * 60 + strftime('%M', wd.shift_end_time)) - 
                        (strftime('%H', wd.shift_start_time) * 60 + strftime('%M', wd.shift_start_time)) -
                        (wd.break_duration + (wd.overtime_hours * 60)) AS work_time
                    FROM 
                        working_day AS wd 
                    JOIN
                        employee_working_day AS ewd ON ewd.working_day_id = wd.working_day_id
                    WHERE work_time <= 0
                ),
                absent AS (
                        SELECT 
                            ewd.employee_id, 
                            em.employee_name, 
                            count(ewd.working_day_id) AS nb_days 
                        FROM 
                            employee_working_day AS ewd 
                        JOIN employee AS em 
                            ON em.employee_id = ewd.employee_id 
                        GROUP BY 
                            ewd.employee_id 
                        HAVING 
                            nb_days < 30
            )

            SELECT
                lo.location_name,
            sum(
                (strftime('%H', wd.shift_end_time) * 60 + strftime('%M', wd.shift_end_time)) - 
                (strftime('%H', wd.shift_start_time) * 60 + strftime('%M', wd.shift_start_time)) -
                wd.break_duration 
                )AS work_time
            FROM
                working_day as wd
            JOIN
                location as lo 
                    on lo.location_id = wd.location
            JOIN
                employee_working_day as ewd 
                    on ewd.working_day_id = wd.working_day_id
            WHERE
                ewd.employee_id NOT IN (SELECT employee_id FROM liar)  
                AND
                ewd.employee_id NOT IN (SELECT employee_id FROM absent)
            GROUP BY
                location
            ORDER BY
                location_name
        """
distribution_of_hours_by_factory = execute_query(query, cursor)
for x in distribution_of_hours_by_factory:
    print(x)

#---------------------------------------------------------------------------#
#------- Liste des 10 employés qui ont fait le plus de R&D par usine -------#
#---------------------------------------------------------------------------#   
print("###### Liste des 10 employés qui ont fait le plus de R&D par usine ######")   
query = "SELECT location_id FROM location"
usine_id = execute_query(query, cursor)
query = """
            WITH liar AS (
                SELECT
                        ewd.employee_id,
                        (strftime('%H', wd.shift_end_time) * 60 + strftime('%M', wd.shift_end_time)) - 
                        (strftime('%H', wd.shift_start_time) * 60 + strftime('%M', wd.shift_start_time)) -
                        (wd.break_duration + (wd.overtime_hours * 60)) AS work_time
                    FROM 
                        working_day AS wd 
                    JOIN
                        employee_working_day AS ewd ON ewd.working_day_id = wd.working_day_id
                    WHERE work_time <= 0
                ),
                absent AS (
                        SELECT 
                            ewd.employee_id, 
                            em.employee_name, 
                            count(ewd.working_day_id) AS nb_days 
                        FROM 
                            employee_working_day AS ewd 
                        JOIN employee AS em 
                            ON em.employee_id = ewd.employee_id 
                        GROUP BY 
                            ewd.employee_id 
                        HAVING 
                            nb_days < 30
            )
            SELECT 
                ewd.employee_id,
                em.employee_name,
                de.department_name,
                sum(
                    (strftime('%H', wd.shift_end_time) * 60 + strftime('%M', wd.shift_end_time)) - 
                    (strftime('%H', wd.shift_start_time) * 60 + strftime('%M', wd.shift_start_time)) -
                    wd.break_duration )AS work_time,
                lo.location_name
            FROM 
                employee as em
            JOIN	
                employee_working_day as ewd
                    on ewd.employee_id = em.employee_id
            JOIN
                department as de
                    on de.department_id = em.department
            JOIN
                working_day as wd
                    on wd.working_day_id = ewd.working_day_id
            JOIN
                location as lo
                    on lo.location_id = wd.location
            WHERE
                ewd.employee_id NOT IN (SELECT employee_id FROM liar)  
                AND
                ewd.employee_id NOT IN (SELECT employee_id FROM absent)
                AND
                de.department_name = 'RD'
                AND
                lo.location_id = (?)
            GROUP By 
                em.employee_id
            ORDER BY
                work_time DESC
            LIMIT 10
"""
tab_top10_RD_by_usine = []
for usine in usine_id:
    result = execute_query(query, cursor, usine)
    if result:
        tab_top10_RD_by_usine.append(result)

for x in tab_top10_RD_by_usine:
    for y in x:
        print(y)
    print("-------")
    
#------------------------------------------------------------------------------------------------#
#------- Liste des 10 employés qui ont pris plus de 50 minutes de pause dans chaque usine -------#
#------------------------------------------------------------------------------------------------#       
print("###### Liste des 10 employés qui ont pris plus de 50 minutes de pause dans chaque usine ######")
query = """
	WITH liar AS (
                    SELECT
                        ewd.employee_id,
                        (strftime('%H', wd.shift_end_time) * 60 + strftime('%M', wd.shift_end_time)) - 
                        (strftime('%H', wd.shift_start_time) * 60 + strftime('%M', wd.shift_start_time)) -
                        (wd.break_duration + (wd.overtime_hours * 60)) AS work_time
                    FROM 
                        working_day AS wd 
                    JOIN
                        employee_working_day AS ewd ON ewd.working_day_id = wd.working_day_id
                    WHERE work_time <= 0
                ),
                absent AS (
                        SELECT 
                            ewd.employee_id, 
                            em.employee_name, 
                            count(ewd.working_day_id) AS nb_days 
                        FROM 
                            employee_working_day AS ewd 
                        JOIN employee AS em 
                            ON em.employee_id = ewd.employee_id 
                        GROUP BY 
                            ewd.employee_id 
                        HAVING 
                            nb_days < 30
                        )
                SELECT 
                    ewd.employee_id,
                    em.employee_name,
                    sum(wd.break_duration) as sum_break,
                    lo.location_name
                FROM 
                    employee as em
                JOIN	
                    employee_working_day as ewd
                        on ewd.employee_id = em.employee_id
                JOIN
                    department as de
                        on de.department_id = em.department
                JOIN
                    working_day as wd
                        on wd.working_day_id = ewd.working_day_id
                JOIN
                    location as lo
                        on lo.location_id = wd.location
                WHERE
                    ewd.employee_id NOT IN (SELECT employee_id FROM liar)  
                    AND
                    ewd.employee_id NOT IN (SELECT employee_id FROM absent)
                    AND
                    wd.break_duration > 50
                    AND
                    lo.location_id = (?)
                GROUP By 
                    em.employee_id
                ORDER BY
                    sum_break DESC
                LIMIT 10
"""
tab_top10_more_break_by_usine = []
for usine in usine_id:
    result = execute_query(query, cursor, usine)
    if result:
        tab_top10_more_break_by_usine.append(result)

for x in tab_top10_more_break_by_usine:
    for y in x:
        print(y)
    print("-------")
    
#-------------------------------------------------------------------------#
#------- Liste des 10 employés qui ont le plus travaillé par usine -------#
#-------------------------------------------------------------------------#       
print("###### Liste des 10 employés qui ont le plus travaillé par usine ######")   
query = """
	WITH liar AS (
                SELECT
                        ewd.employee_id,
                        (strftime('%H', wd.shift_end_time) * 60 + strftime('%M', wd.shift_end_time)) - 
                        (strftime('%H', wd.shift_start_time) * 60 + strftime('%M', wd.shift_start_time)) -
                        (wd.break_duration + (wd.overtime_hours * 60)) AS work_time
                    FROM 
                        working_day AS wd 
                    JOIN
                        employee_working_day AS ewd ON ewd.working_day_id = wd.working_day_id
                    WHERE work_time <= 0
                ),
                absent AS (
                        SELECT 
                            ewd.employee_id, 
                            em.employee_name, 
                            count(ewd.working_day_id) AS nb_days 
                        FROM 
                            employee_working_day AS ewd 
                        JOIN employee AS em 
                            ON em.employee_id = ewd.employee_id 
                        GROUP BY 
                            ewd.employee_id 
                        HAVING 
                            nb_days < 30
                )
                SELECT 
                    ewd.employee_id,
                    em.employee_name,
                    sum(
                        (strftime('%H', wd.shift_end_time) * 60 + strftime('%M', wd.shift_end_time)) - 
                        (strftime('%H', wd.shift_start_time) * 60 + strftime('%M', wd.shift_start_time)) -
                        wd.break_duration )AS work_time,
                    lo.location_name
                FROM 
                    employee as em
                JOIN	
                    employee_working_day as ewd
                        on ewd.employee_id = em.employee_id
                JOIN
                    department as de
                        on de.department_id = em.department
                JOIN
                    working_day as wd
                        on wd.working_day_id = ewd.working_day_id
                JOIN
                    location as lo
                        on lo.location_id = wd.location
                WHERE
                    ewd.employee_id NOT IN (SELECT employee_id FROM liar)  
                    AND
                    ewd.employee_id NOT IN (SELECT employee_id FROM absent)
                    AND
                    lo.location_id= (?)
                GROUP By 
                    em.employee_id
                ORDER BY
                    work_time DESC
                LIMIT 	10
"""
tab_top10_more_work_time_by_usine = []
for usine in usine_id:
    result = execute_query(query, cursor, usine)
    if result:
        tab_top10_more_work_time_by_usine.append(result)

for x in tab_top10_more_work_time_by_usine:
    for y in x:
        print(y)
    print("-------")
    
#---------------------------------------------------------------------#
#------- Combien d’heure supplémentaires ont été travaillées ? -------#
#---------------------------------------------------------------------#       
print("###### Combien d’heure supplémentaires ont été travaillées ? ######")   
query = """
	WITH liar AS (
                SELECT
                        ewd.employee_id,
                        (strftime('%H', wd.shift_end_time) * 60 + strftime('%M', wd.shift_end_time)) - 
                        (strftime('%H', wd.shift_start_time) * 60 + strftime('%M', wd.shift_start_time)) -
                        (wd.break_duration + (wd.overtime_hours * 60)) AS work_time
                    FROM 
                        working_day AS wd 
                    JOIN
                        employee_working_day AS ewd ON ewd.working_day_id = wd.working_day_id
                    WHERE work_time <= 0
                ),
                absent AS (
                        SELECT 
                            ewd.employee_id, 
                            em.employee_name, 
                            count(ewd.working_day_id) AS nb_days 
                        FROM 
                            employee_working_day AS ewd 
                        JOIN employee AS em 
                            ON em.employee_id = ewd.employee_id 
                        GROUP BY 
                            ewd.employee_id 
                        HAVING 
                            nb_days < 30
                )
                SELECT 
                    sum(wd.overtime_hours)
                FROM 
                    working_day as wd
                JOIN	
                    employee_working_day as ewd
                        on ewd.working_day_id = wd.working_day_id
                WHERE
                    ewd.employee_id NOT IN (SELECT employee_id FROM liar)  
                    AND
                    ewd.employee_id NOT IN (SELECT employee_id FROM absent)
                """
total_overtime_hour = execute_query(query, cursor)
print(total_overtime_hour[0][0])

#---------------------------------------------------------------------------------#
#------- Y a-t-il un lien entre heures supplémentaires et temps de pause ? -------#
#---------------------------------------------------------------------------------#       
print("###### Y a-t-il un lien entre heures supplémentaires et temps de pause ? ######")
query ="""
	WITH liar AS (
                SELECT
                        ewd.employee_id,
                        (strftime('%H', wd.shift_end_time) * 60 + strftime('%M', wd.shift_end_time)) - 
                        (strftime('%H', wd.shift_start_time) * 60 + strftime('%M', wd.shift_start_time)) -
                        (wd.break_duration + (wd.overtime_hours * 60)) AS work_time
                    FROM 
                        working_day AS wd 
                    JOIN
                        employee_working_day AS ewd ON ewd.working_day_id = wd.working_day_id
                    WHERE work_time <= 0
                ),
                absent AS (
                        SELECT 
                            ewd.employee_id, 
                            em.employee_name, 
                            count(ewd.working_day_id) AS nb_days 
                        FROM 
                            employee_working_day AS ewd 
                        JOIN employee AS em 
                            ON em.employee_id = ewd.employee_id 
                        GROUP BY 
                            ewd.employee_id 
                        HAVING 
                            nb_days < 30
                )
                SELECT 
                    ewd.working_day_date,
                    sum(wd.break_duration) as sum_break,
                    sum(wd.overtime_hours * 60) as sum_overtime
                FROM
                    working_day as wd
                JOIN 
                    employee_working_day as ewd
                        on ewd.working_day_id = wd.working_day_id
                JOIN
                    employee as em
                        on em.employee_id = ewd.employee_id
                WHERE
                    ewd.employee_id NOT IN (SELECT employee_id FROM liar)  
                    AND
                    ewd.employee_id NOT IN (SELECT employee_id FROM absent)
                GROUP BY 
                    ewd.working_day_date
"""
daily_overtime_and_daily_break_time = execute_query(query, cursor)

table_x = []
table_y = []
for x in daily_overtime_and_daily_break_time:
    table_x.append(x[1])
    table_y.append(x[2])
print(stats.pearsonr(table_x,table_y))

#-------------------------------------------------------------------------------------#
#------- Y a-t-il un lien entre heures supplémentaires et heures travaillées ? -------#
#-------------------------------------------------------------------------------------#       
print("###### Y a-t-il un lien entre heures supplémentaires et heures travaillées ? ######")
query ="""
	WITH liar AS (
                SELECT
                        ewd.employee_id,
                        (strftime('%H', wd.shift_end_time) * 60 + strftime('%M', wd.shift_end_time)) - 
                        (strftime('%H', wd.shift_start_time) * 60 + strftime('%M', wd.shift_start_time)) -
                        (wd.break_duration + (wd.overtime_hours * 60)) AS work_time
                    FROM 
                        working_day AS wd 
                    JOIN
                        employee_working_day AS ewd ON ewd.working_day_id = wd.working_day_id
                    WHERE work_time <= 0
                ),
                absent AS (
                        SELECT 
                            ewd.employee_id, 
                            em.employee_name, 
                            count(ewd.working_day_id) AS nb_days 
                        FROM 
                            employee_working_day AS ewd 
                        JOIN employee AS em 
                            ON em.employee_id = ewd.employee_id 
                        GROUP BY 
                            ewd.employee_id 
                        HAVING 
                            nb_days < 30
                )
                SELECT 
                    ewd.working_day_date,
					(strftime('%H', wd.shift_end_time) * 60 + strftime('%M', wd.shift_end_time)) - 
					(strftime('%H', wd.shift_start_time) * 60 + strftime('%M', wd.shift_start_time)) -
					(wd.break_duration + (wd.overtime_hours * 60)) AS work_time,
                    sum(wd.overtime_hours * 60) as sum_overtime
                FROM
                    working_day as wd
                JOIN 
                    employee_working_day as ewd
                        on ewd.working_day_id = wd.working_day_id
                JOIN
                    employee as em
                        on em.employee_id = ewd.employee_id
                WHERE
                    ewd.employee_id NOT IN (SELECT employee_id FROM liar)  
                    AND
                    ewd.employee_id NOT IN (SELECT employee_id FROM absent)
                GROUP BY 
                    ewd.working_day_date
    """
daily_work_time_and_daily_break_time = execute_query(query, cursor)

table_x = []
table_y = []
for x in daily_work_time_and_daily_break_time:
    table_x.append(x[1])
    table_y.append(x[2])
print(stats.pearsonr(table_x,table_y))

#-----------------------------------------------------------------------------#
#------- Y a-t-il un lien entre heures travaillées et temps de pause ? -------#
#-----------------------------------------------------------------------------#       
print("###### Y a-t-il un lien entre heures travaillées et temps de pause ? ######")
query ="""
	WITH liar AS (
                SELECT
                        ewd.employee_id,
                        (strftime('%H', wd.shift_end_time) * 60 + strftime('%M', wd.shift_end_time)) - 
                        (strftime('%H', wd.shift_start_time) * 60 + strftime('%M', wd.shift_start_time)) -
                        (wd.break_duration + (wd.overtime_hours * 60)) AS work_time
                    FROM 
                        working_day AS wd 
                    JOIN
                        employee_working_day AS ewd ON ewd.working_day_id = wd.working_day_id
                    WHERE work_time <= 0
                ),
                absent AS (
                        SELECT 
                            ewd.employee_id, 
                            em.employee_name, 
                            count(ewd.working_day_id) AS nb_days 
                        FROM 
                            employee_working_day AS ewd 
                        JOIN employee AS em 
                            ON em.employee_id = ewd.employee_id 
                        GROUP BY 
                            ewd.employee_id 
                        HAVING 
                            nb_days < 30
                )
                SELECT 
                    ewd.working_day_date,
					sum(
					(strftime('%H', wd.shift_end_time) * 60 + strftime('%M', wd.shift_end_time)) - 
					(strftime('%H', wd.shift_start_time) * 60 + strftime('%M', wd.shift_start_time)) -
					(wd.break_duration + (wd.overtime_hours * 60))) AS work_time,
                    sum(wd.break_duration) as sum_break
                FROM
                    working_day as wd
                JOIN 
                    employee_working_day as ewd
                        on ewd.working_day_id = wd.working_day_id
                JOIN
                    employee as em
                        on em.employee_id = ewd.employee_id
                WHERE
                    ewd.employee_id NOT IN (SELECT employee_id FROM liar)  
                    AND
                    ewd.employee_id NOT IN (SELECT employee_id FROM absent)
                GROUP BY 
                    ewd.working_day_date
    """

daily_break_time_and_daily_break_time = execute_query(query, cursor)

table_x = []
table_y = []
for x in daily_break_time_and_daily_break_time:
    table_x.append(x[1])
    table_y.append(x[2])
print(stats.pearsonr(table_x,table_y))

#-----------------------------------------#
#------- Où a eu lieu l’incendie ? -------#
#-----------------------------------------#       
print("###### Où a eu lieu l’incendie ? ######")
query ="""
		WITH liar AS (
                SELECT
                        ewd.employee_id,
                        (strftime('%H', wd.shift_end_time) * 60 + strftime('%M', wd.shift_end_time)) - 
                        (strftime('%H', wd.shift_start_time) * 60 + strftime('%M', wd.shift_start_time)) -
                        (wd.break_duration + (wd.overtime_hours * 60)) AS work_time
                    FROM 
                        working_day AS wd 
                    JOIN
                        employee_working_day AS ewd ON ewd.working_day_id = wd.working_day_id
                    WHERE work_time <= 0
                ),
                absent AS (
                        SELECT 
                            ewd.employee_id, 
                            em.employee_name, 
                            count(ewd.working_day_id) AS nb_days 
                        FROM 
                            employee_working_day AS ewd 
                        JOIN employee AS em 
                            ON em.employee_id = ewd.employee_id 
                        GROUP BY 
                            ewd.employee_id 
                        HAVING 
                            nb_days < 30
                ),false_data AS (
                   SELECT
                        wd.working_day_id,
                        (strftime('%H', wd.shift_end_time) * 60 + strftime('%M', wd.shift_end_time)) - 
                        (strftime('%H', wd.shift_start_time) * 60 + strftime('%M', wd.shift_start_time)) AS work_time
                    FROM 
                        working_day AS wd 
                    JOIN
                        employee_working_day AS ewd ON ewd.working_day_id = wd.working_day_id
                    WHERE work_time < 0 
                )
                SELECT 
                    ewd.working_day_date,
					sum(
					(strftime('%H', wd.shift_end_time) * 60 + strftime('%M', wd.shift_end_time)) - 
					(strftime('%H', wd.shift_start_time) * 60 + strftime('%M', wd.shift_start_time))) AS work_time
                FROM
                    working_day as wd
                JOIN 
                    employee_working_day as ewd
                        on ewd.working_day_id = wd.working_day_id
                JOIN
                    employee as em
                        on em.employee_id = ewd.employee_id
                JOIN
                    location as lo
                        on lo.location_id = wd.location
                WHERE
                    wd.working_day_id NOT IN (SELECT working_day_id FROM false_data)  
					AND
					lo.location_id = (?)
                GROUP BY 
                    ewd.working_day_date
    """
    
list_usine = select_usine(cursor)
work_time_in_usine = []
for usine in list_usine:
    print(usine)
    temp = execute_query(query, cursor, usine)
    work_time_in_usine.append(temp)

for x in work_time_in_usine:
    print(x)

#----------------------------------#
#------- Qui a mis le feu ? -------#
#----------------------------------#       
print("###### Qui a mis le feu ? ######")  
query ="""
        	WITH liar AS (
                SELECT
                        ewd.employee_id,
                        (strftime('%H', wd.shift_end_time) * 60 + strftime('%M', wd.shift_end_time)) - 
                        (strftime('%H', wd.shift_start_time) * 60 + strftime('%M', wd.shift_start_time)) -
                        (wd.break_duration + (wd.overtime_hours * 60)) AS work_time
                    FROM 
                        working_day AS wd 
                    JOIN
                        employee_working_day AS ewd ON ewd.working_day_id = wd.working_day_id
                    WHERE work_time <= 0
                ),absent AS (
                        SELECT 
                            ewd.employee_id, 
                            em.employee_name, 
                            count(ewd.working_day_id) AS nb_days 
                        FROM 
                            employee_working_day AS ewd 
                        JOIN employee AS em 
                            ON em.employee_id = ewd.employee_id 
                        GROUP BY 
                            ewd.employee_id 
                        HAVING 
                            nb_days < 30
                ),false_data AS (
                   SELECT
                        wd.working_day_id,
                        (strftime('%H', wd.shift_end_time) * 60 + strftime('%M', wd.shift_end_time)) - 
                        (strftime('%H', wd.shift_start_time) * 60 + strftime('%M', wd.shift_start_time)) AS work_time
                    FROM 
                        working_day AS wd 
                    JOIN
                        employee_working_day AS ewd ON ewd.working_day_id = wd.working_day_id
                    WHERE work_time < 0 
                )
                SELECT 
                    ewd.employee_id,
					em.employee_name,
					lo.location_name,
					ewd.working_day_date,
					max(shift_end_time)
                FROM
                    working_day as wd
                JOIN 
                    employee_working_day as ewd
                        on ewd.working_day_id = wd.working_day_id
                JOIN
                    employee as em
                        on em.employee_id = ewd.employee_id
				JOIN 
					location as lo
						on lo.location_id = wd.location
                WHERE
                    wd.working_day_id NOT IN (SELECT working_day_id FROM false_data)  
					AND
					lo.location_name = "Usine A"
                GROUP BY 
                    ewd.working_day_date
				ORDER BY 
					ewd.working_day_date DESC
				LIMIT 1			
"""

the_last_employee_of_factory_A = execute_query(query, cursor)
print(the_last_employee_of_factory_A)

#------------------------------------------------------------------------#
#------- Quel a été l’influence de l’incendie sur la production ? -------#
#------------------------------------------------------------------------#       
print("###### Quel a été l’influence de l’incendie sur la production ? ######")  
query ="""
	WITH liar AS (
                SELECT
                        ewd.employee_id,
                        (strftime('%H', wd.shift_end_time) * 60 + strftime('%M', wd.shift_end_time)) - 
                        (strftime('%H', wd.shift_start_time) * 60 + strftime('%M', wd.shift_start_time)) -
                        (wd.break_duration + (wd.overtime_hours * 60)) AS work_time
                    FROM 
                        working_day AS wd 
                    JOIN
                        employee_working_day AS ewd ON ewd.working_day_id = wd.working_day_id
                    WHERE work_time <= 0
                ),absent AS (
                        SELECT 
                            ewd.employee_id, 
                            em.employee_name, 
                            count(ewd.working_day_id) AS nb_days 
                        FROM 
                            employee_working_day AS ewd 
                        JOIN employee AS em 
                            ON em.employee_id = ewd.employee_id 
                        GROUP BY 
                            ewd.employee_id 
                        HAVING 
                            nb_days < 30
                ),false_data AS (
                   SELECT
                        wd.working_day_id,
                        (strftime('%H', wd.shift_end_time) * 60 + strftime('%M', wd.shift_end_time)) - 
                        (strftime('%H', wd.shift_start_time) * 60 + strftime('%M', wd.shift_start_time)) AS work_time
                    FROM 
                        working_day AS wd 
                    JOIN
                        employee_working_day AS ewd ON ewd.working_day_id = wd.working_day_id
                    WHERE work_time < 0 
                )
                SELECT 
					ewd.working_day_date,
					sum((strftime('%H', wd.shift_end_time) * 60 + strftime('%M', wd.shift_end_time)) - 
					(strftime('%H', wd.shift_start_time) * 60 + strftime('%M', wd.shift_start_time)) -
					(wd.break_duration)) AS work_time
                FROM
                    working_day as wd
                JOIN 
                    employee_working_day as ewd
                        on ewd.working_day_id = wd.working_day_id
                JOIN
                    employee as em
                        on em.employee_id = ewd.employee_id
				JOIN 
					location as lo
						on lo.location_id = wd.location
                WHERE
                    wd.working_day_id NOT IN (SELECT working_day_id FROM false_data)  
                GROUP BY 
                    ewd.working_day_date
					
"""
daily_business_activity = execute_query(query,cursor)
print(daily_business_activity)

#-------------------------------------------#
#------- Qui est l’employé du mois ? -------#
#-------------------------------------------#       
print("###### Qui est l’employé du mois ? ######")  

# for salary in salary_list:   
#     list = execute_query(query, cursor, salary)
#     salary_list_of_employees = salary_list_of_employees + list

# for x in salary_list_employees_with_prime:
#     print(x)
# print(salary_list_employees_with_prime)

employees_sorted = sorted(salary_list_employees_with_prime, key=lambda x: (-x['work_time'], x['total_salary']))[:2]
for x in employees_sorted:
    print(x)
# print(employees_sorted)